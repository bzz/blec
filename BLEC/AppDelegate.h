//
//  AppDelegate.h
//  BLEC
//
//  Created by Mikhail Baynov on 11/12/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//




///USE Terminal mb$   uuidgen
#define TRANSFER_SERVICE_UUID           @"E20A39F4-73F5-4BC4-A12F-17D1AD07A961"
#define TRANSFER_CHARACTERISTIC_UUID    @"08590F7E-DB05-467E-8757-72F6FAEB13D4"
#define NOTIFY_MTU      20



#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Central.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) Central *central;


@property (strong, nonatomic) NSMutableData *dataToSend;
@property (strong, nonatomic) NSMutableString *recievedText;
@property (strong, nonatomic) NSMutableString *name;






- (NSURL *)applicationDocumentsDirectory;

- (void)showAlert:(NSString *)title message:(NSString *)message;

@end

