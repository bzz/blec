//
//  BLEDevice.h
//  BTLE Transfer
//
//  Created by Mikhail Baynov on 11/12/14.
//  Copyright (c) 2014 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>



@interface BLEDevice : NSObject <CBPeripheralManagerDelegate>

@property (strong, nonatomic) CBPeripheralManager       *peripheralManager;
@property (strong, nonatomic) CBMutableCharacteristic   *transferCharacteristic;
@property (nonatomic, readwrite) NSInteger              sendDataIndex;


@end
