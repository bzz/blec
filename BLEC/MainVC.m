//
//  MainVC.m
//  BLEC
//
//  Created by Mikhail Baynov on 26/12/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import "MainVC.h"
#import "AppDelegate.h"



@interface MainVC ()
{
    AppDelegate *appDelegate;
    NSNotificationCenter *notificationCenter;
}
@end

@implementation MainVC


- (void)loadView
{
    self.view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil][0];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.textField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self selector:@selector(data) name:@"data" object:nil];
    [notificationCenter addObserver:self selector:@selector(unsubscribed) name:@"unsubscribed" object:nil];
    //    _central = [Central new];
    _bledevice = [BLEDevice new];
    [self.textField becomeFirstResponder];
    
    [notificationCenter addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    
}


- (void)data
{
    _textView.text = [NSString stringWithFormat:@"%@\n%@", self.textView.text, appDelegate.recievedText];
    CGPoint bottomOffset = CGPointMake(0, _textView.contentSize.height - _textView.bounds.size.height);
    [_textView setContentOffset:bottomOffset animated:YES];
}

- (void)unsubscribed
{
    [self.advertisingSwitch setOn:NO animated:YES];
    self.button.selected = NO;
    [self.bledevice.peripheralManager stopAdvertising];
    self.textField.text = nil;
}


- (void)viewWillDisappear:(BOOL)animated
{
    //    [self.central.centralManager stopScan];
    [super viewWillDisappear:animated];
}



- (IBAction)sendButtonPressed:(UIButton *)sender
{
    if (!self.button.selected) {
        self.button.selected = YES;
        if (_advertisingSwitch.on == NO) {
            NSString *str = [NSString stringWithFormat:@"%@: %@", appDelegate.name, self.textField.text];
            appDelegate.dataToSend = [str dataUsingEncoding:NSUTF8StringEncoding].mutableCopy;
            [self.advertisingSwitch setOn:YES animated:YES];
            [self.bledevice.peripheralManager startAdvertising:
             @{ CBAdvertisementDataServiceUUIDsKey : @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]] }];
        }
        self.textView.text = [NSString stringWithFormat:@"%@\nMe: %@", self.textView.text, self.textField.text];
    }
}




- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self sendButtonPressed:self.button];
    return NO;
}

- (void)keyboardWasShown:(NSNotification*)notification
{
    self.bottomOffsetConstraint.constant = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
}


@end
