//
//  SettingsVC.m
//  BLEC
//
//  Created by Mikhail Baynov on 25/12/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import "SettingsVC.h"

@interface SettingsVC ()

@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];


}


- (IBAction)imageButtonPressed:(UIButton *)sender {
    //    [self removeInputViewsAndKeyboard:YES];
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        
        CGSize newSize = self.imageButton.frame.size;
        newSize.height *= 3;
        newSize.width  *= 3;
        
        UIGraphicsBeginImageContext(newSize);
        [info[UIImagePickerControllerEditedImage] drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
        UIImage *userImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        ///        appDelegate.userData.userImage = info[UIImagePickerControllerEditedImage];
        [self.imageButton setBackgroundImage:userImage forState:UIControlStateNormal];
    }];
}


@end
