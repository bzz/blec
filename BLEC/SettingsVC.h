//
//  SettingsVC.h
//  BLEC
//
//  Created by Mikhail Baynov on 25/12/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UIViewController
<UIImagePickerControllerDelegate, UINavigationControllerDelegate>



@property (weak, nonatomic) IBOutlet UIButton *imageButton;
@property (weak, nonatomic) IBOutlet UIImageView *secondImageView;


@end
