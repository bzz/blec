//
//  AppDelegate.m
//  BLEC
//
//  Created by Mikhail Baynov on 11/12/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import "AppDelegate.h"
#import "MainVC.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = [MainVC new];
    [self.window makeKeyAndVisible];
    
    self.central = [Central new];
    self.dataToSend = [NSMutableData new];
    self.name = [[UIDevice currentDevice] name].mutableCopy;
    
    
    return YES;
}






- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)showAlert:(NSString *)title message:(NSString *)message
{
    [[[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

@end
