//
//  MainVC.h
//  BLEC
//
//  Created by Mikhail Baynov on 26/12/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Central.h"
#import "BLEDevice.h"



@interface MainVC : UIViewController

<UITextViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) Central *central;
@property (strong, nonatomic) BLEDevice *bledevice;

@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UISwitch *advertisingSwitch;
@property (weak, nonatomic) IBOutlet UIButton *button;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomOffsetConstraint;

@end
